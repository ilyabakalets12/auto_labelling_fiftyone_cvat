from bing_image_downloader import downloader
import argparse

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-img", "--image_class", default='traffic_road', help="class images you need")
	parser.add_argument("-lim", "--image_limit", default=20, help="number of images")
	parser.add_argument("-t", "--timeout", default=60)
	parser.add_argument("-v", "--verbose", default=False)

	args = parser.parse_args()

	downloader.download(args.image_class, limit=args.image_limit, output_dir='dataset', adult_filter_off=True,
	                    force_replace=False,
	                    timeout=args.timeout, verbose=False)
